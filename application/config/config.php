<?php

/**
 * DEVELOPMENT
 */
if (ENV === 'DEV') {
    $config['db_host'] = 'localhost'; // Database host (e.g. localhost)
    $config['db_name'] = 'douglasmk'; // Database name
    $config['db_username'] = 'd0u9145mk'; // Database username
    $config['db_password'] = 'z3aqmHbHsm2dWn6S'; // Database password
} else {
    /**
     * PRODUCTION
     */
    $config['db_host'] = 'localhost'; // Database host (e.g. localhost)
    $config['db_name'] = 'dougdmk_douglasmk'; // Database name
    $config['db_username'] = 'dougdmk_d0u9145'; // Database username
    $config['db_password'] = 'z3aqmHbHsm2dWn6S'; // Database password
}

$config['base_url'] = './'; // Base URL including trailing slash (e.g. http://localhost/)
$config['default_controller'] = 'main'; // Default controller to load
$config['error_controller'] = 'error'; // Controller used for errors (e.g. 404, 500 etc)