<?php

class portfolio extends Model {

    public function getAll() {
        $result = $this->query('SELECT * FROM dmk_portfolio ORDER BY por_ordem DESC');


        foreach ($result as $i => $item) {
            $item->categorias = $this->query('SELECT cat_nome FROM dmk_categorias
					INNER JOIN dmk_rel_port_cat ON cat_id=rpc_id_categoria
					WHERE rpc_id_portfolio = ' . $item->por_id);
            $result[$i] = $item;
        }

        return $result;
    }

    public function getByInnerUrl($url) {

        $result = $this->query('SELECT * FROM dmk_portfolio WHERE por_internal_url="' . trim($url) . '" LIMIT 1');
        
        $result[0]->categorias = $this->query('SELECT cat_nome FROM dmk_categorias
					INNER JOIN dmk_rel_port_cat ON cat_id=rpc_id_categoria
					WHERE rpc_id_portfolio = ' . $result->por_id);
        
        return $result;
    }

}

?>
