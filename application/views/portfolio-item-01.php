<!DOCTYPE html>
<html lang="en" class="no-js single-page-layout" data-classic-layout="false" data-mobile-only-classic-layout="true" data-inanimation="fadeInUp" data-outanimation="fadeOutDownBig"><!-- InstanceBegin template="/Templates/layout.dwt" codeOutsideHTMLIsLocked="false" -->

    <head>

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="cvCard - Responsive HTML5 vCard Template" />
        <meta name="keywords" content="vcard, html5, portfolio" />
        <meta name="author" content="Pixelwars" />

        <title>cvCard</title>

        <!-- FAV and TOUCH ICONS -->
        <link rel="shortcut icon" href="./images/ico/favicon.ico" />
        <link rel="apple-touch-icon" href="./images/ico/apple-touch-icon.png" />

        <script src="./js/modernizr.custom.js"></script>

        <!-- FONTS -->
        <link href='../../fonts.googleapis.com/css_8e1dc5f6.css' rel='stylesheet' type='text/css' />
        <link href='../../fonts.googleapis.com/css_d7b56e77.css' rel='stylesheet' type='text/css' />
        <link href='../../fonts.googleapis.com/css_adb7fa49.css' rel='stylesheet' type='text/css' />

        <!-- STYLES -->
        <link rel="stylesheet" type="text/css" media="print" href="./css/print.css" />
        <link rel="stylesheet" type="text/css" href="./css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="./js/nprogress/nprogress.css" />
        <link rel="stylesheet" type="text/css" href="./css/animate.css" />
        <link rel="stylesheet" type="text/css" href="./css/fonts/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="./css/fonts/fontello/css/fontello.css" />
        <link rel="stylesheet" type="text/css" href="./css/jquery.fancybox-1.3.4.css" />
        <link rel="stylesheet" type="text/css" href="./js/google-code-prettify/prettify.css" />
        <link rel="stylesheet" type="text/css" href="./css/uniform.default.css" />
        <link rel="stylesheet" type="text/css" href="./css/flexslider.css" />
        <link rel="stylesheet" type="text/css" href="./js/mediaelement/mediaelementplayer.css" />
        <link rel="stylesheet" type="text/css" href="./css/tooltipster.css" />
        <link rel="stylesheet" type="text/css" href="./css/style.css" />
        <link rel="stylesheet" type="text/css" href="./css/main.css" />

        <!--[if lte IE 9]>
            <script src="./js/html5shiv.js"></script>
            <script src="./js/respond.min.js"></script>
            <script src="./js/selectivizr-min.js"></script>
        <![endif]--> 

        <link rel="stylesheet" type="text/css" href="./style-switcher/style.css" />
        <link rel="stylesheet" class="base-skin" type="text/css" href="#" />


        <!-- InstanceBeginEditable name="head" -->
        <!-- InstanceEndEditable -->

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>



    <body>




        <!-- CONTAINER -->
        <div class="container" id="container">



            <!-- HEADER -->
            <header class="header">

                <img src="./images/site/avatar.png" alt="avatar" />
                <h1>Johnny Doe</h1>
                <p>freelance graphic designer</p>

                <!-- NAV MENU -->
                <ul class="vs-nav">

                    <!-- InstanceBeginEditable name="Menu-Content" -->

                    <li><a href="./index.html#/portfolio"><i title="back to portfolio" class="icon-left-open tooltip" data-tooltip-pos="left"></i></a></li>
                    <li><a href="./index.html"><i title="go to home" class="icon-home-1 tooltip" data-tooltip-pos="right"></i></a></li>

                    <!-- InstanceEndEditable -->

                </ul>
                <!-- NAV MENU -->

                <!-- SEARCH --> 
                <div class="header-search">
                    <form role="search" method="get" id="search-form" action="#" />
                    <input type="text" value="" name="s" id="search" placeholder="ENTER KEYWORD" />
                    <input type="submit" id="search-submit" title="Search" value="→" />
                    </form>
                </div>
                <!-- SEARCH -->

            </header>
            <!-- HEADER -->





            <!-- WRAPPER -->
            <div class="wrapper">



                <!-- InstanceBeginEditable name="Page-Content" -->


                <!-- .portfolio-single -->
                <div class="portfolio-single">

                    <!-- .row -->
                    <div class="row">

                        <!-- TITLE - column 8/12 -->
                        <div class="col-md-8 portfolio-field portfolio-title">
                            <h2><?php echo $item->por_titulo; ?></h2>
                        </div>
                        <!-- TITLE - column 8/12 -->


                        <!-- PORTFOLIO-NAV - column 4/12 -->
                        <div class="col-md-4 portfolio-field portfolio-nav">
                            <a class="button back" href="#/portfolio"></a>
                        </div>
                        <!-- PORTFOLIO-NAV - column 4/12 -->

                    </div>
                    <!-- .row -->



                    <!-- .row -->
                    <div class="row">

                        <!-- PORTFOLIO-IMAGES - column 8/12 -->
                        <div class="col-md-8 portfolio-field">
                            <img src="./images/portfolio/single/single-04.jpg" alt="project" />
                            <img src="./images/portfolio/single/single-03.jpg" alt="project" />
                        </div>
                        <!-- PORTFOLIO-IMAGES - column 8/12 -->


                        <!-- PORTFOLIO SIDEBAR - column 4/12 -->
                        <div class="col-md-4 portfolio-field">


                            <h3>Descrição</h3>

                            <p><?php echo $item->por_descricao; ?></p>

                            <h3>Categorias</h3>
                            <ul class="tags">
                                <?php
                                foreach ($item->categorias as $categoria) {
                                    echo "<li><a>$categoria->cat_nome</a></li>";
                                }
                                ?>
                            </ul>

                            <div class="launch">
                                <a href="<?php echo $item->por_url; ?>" target="blank" class="button primary">VISITAR O SITE</a>
                            </div>


                        </div>
                        <!-- PORTFOLIO SIDEBAR - column 4/12 --> 

                    </div>
                    <!-- .row -->


                    <!-- .row -->
                    <div class="row">

                        <!-- PORTFOLIO-NAV BOTTOM - column 12/12 -->
                        <div class="col-md-12 portfolio-field portfolio-nav bottom">
                            <a class="button back" href="#/portfolio"></a>
                        </div>
                        <!-- PORTFOLIO-NAV BOTTOM - column 12/12 -->  

                    </div>
                    <!-- .row -->



                </div>
                <!-- .portfolio-single -->



                <!-- InstanceEndEditable -->



            </div>
            <!-- WRAPPER -->


        </div>
        <!-- CONTAINER -->





        <!-- InstanceBeginEditable name="Body-End-Content" -->



        <!-- InstanceEndEditable -->




        <!-- SCRIPTS -->
        <script src="./js/jquery-1.10.2.min.js"></script>
        <script src="./js/jquery-migrate-1.2.1.min.js"></script>
        <script src="./js/jquery.address-1.5.min.js"></script>
        <script src="./js/triple.layout.js"></script>
        <script src="./js/smoothscroll.js"></script>
        <script src="./js/nprogress/nprogress.js"></script>
        <script src="./js/fastclick.js"></script>
        <script src="./js/jquery.imagesloaded.min.js"></script>
        <script src="./js/jquery.isotope.min.js"></script>
        <script src="./js/jquery.flexslider-min.js"></script>
        <script src="./js/jquery.fitvids.js"></script>
        <script src="./js/jquery.validate.min.js"></script>
        <script src="./js/jquery.uniform.min.js"></script>
        <script src="./js/jquery.fancybox-1.3.4.pack.js"></script>
        <script src="./js/jquery.tooltipster.min.js"></script>
        <script src="./js/google-code-prettify/prettify.js"></script>
        <script src="./js/twitterFetcher_v10_min.js"></script>
        <script src="./js/mediaelement/mediaelement-and-player.min.js"></script>
        <script src="./js/send-mail.js"></script>
        <script src="./js/classie.js"></script>
        <script src="../../maps.googleapis.com/maps/api/js_3c9fd14e.js"></script>
        <script src="./js/main.js"></script>


    </body>
    <!-- InstanceEnd --></html>