<section id="portfolio" class="portfolio page">
    <div class="content">


        <h2 class="section-title center"><span><i class="icon-leaf"></i>MY WORKS</span></h2>

        <!--FILTERS-->
        <ul id="filters" class="filters">
            <li class="current"><a href="#" data-filter="*">TODOS</a></li>
            <li><a href="#" data-filter=".design">DESIGN</a></li>
            <li><a href="#" data-filter=".desenvolvimento">DESENVOLVIMENTO</a></li>
            <li><a href="#" data-filter=".logo">LOGO</a></li>
        </ul>
        <!--FILTERS-->



        <!-- PORTFOLIO -->
        <div class="portfolio-items media-grid" data-layout="masonry">


            <?php
            foreach ($itensPortfolio as $portfolio) {
                $categorias = '';
                foreach ($portfolio->categorias as $categoria) {
                    $categorias .= mb_strtolower($categoria->cat_nome) . ' ';
                }
                ?>
                <!-- portfolio-item -->
                <div class="media-cell <?php echo $categorias; ?> hentry">

                    <div class="media-box">
                        <img src="<?php echo BASE_URL . 'static/images/portfolio/' . $portfolio->por_thumb; ?>" alt="portfolio-post" />
                        <div class="mask">
                            <a href="teste" class="ajax"></a>
                        </div>
                    </div>

                    <div class="media-cell-desc">
                        <h3><?php echo ($portfolio->por_titulo); ?></h3>
                        <p class="category"><?php echo $portfolio->por_descricao; ?></p>
                    </div>

                </div>
                <!-- portfolio-item -->
                <?php
            }
            ?>

        </div>
        <!-- PORTFOLIO -->






    </div>
    <!-- CONTENT -->

</section>