<!DOCTYPE html>
<html lang="pt-br" class="no-js one-page-layout" data-classic-layout="false" data-mobile-only-classic-layout="true" data-inanimation="fadeInUp" data-outanimation="fadeOutDownBig"><!-- InstanceBegin template="/Templates/layout.dwt" codeOutsideHTMLIsLocked="false" -->

    <head>

        <meta charset="utf-8" />

        <meta name="theme-color" content="#345" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="cvCard - Responsive HTML5 vCard Template" />
        <meta name="publisher" content="Douglas Martinello Karling"/>
        <meta name="copyright" content="Douglas Martinello Karling - www.douglasmk.com.br"/>
        <meta name="author" content="Douglas Martinello Karling (douglasmartinello@gmail.com / douglasmk.com.br)" />
        <meta name="resource-type" content="document" />
        <meta name="classification" content="Web developer" />
        <meta name="robots" content="ALL"/>
        <meta name="revisit-after" content="7 days"/>
        <meta name="distribution" content="Global" />
        <meta name="rating" content="General" />
        <meta name="language" content="pt-br" />
        <meta name="doc-class" content="Completed" />
        <meta name="doc-rights" content="Private" />
        <meta name="keywords" content="desenvolvimento web, webdesign, sites, websites, logomarca, logotipo, logo" />
        <meta name="title" content="DouglasMK - desenvolvimento web" />

        <title>DouglasMK - Desenvolvedor Web</title>

        <!-- FAV and TOUCH ICONS -->

        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo BASE_URL; ?>static/images/ico/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo BASE_URL; ?>static/images/ico/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo BASE_URL; ?>static/images/ico/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo BASE_URL; ?>static/images/ico/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo BASE_URL; ?>static/images/ico/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo BASE_URL; ?>static/images/ico/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo BASE_URL; ?>static/images/ico/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo BASE_URL; ?>static/images/ico/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo BASE_URL; ?>static/images/ico/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo BASE_URL; ?>static/images/ico/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo BASE_URL; ?>static/images/ico/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo BASE_URL; ?>static/images/ico/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo BASE_URL; ?>static/images/ico/favicon-16x16.png">
        <link rel="manifest" href="<?php echo BASE_URL; ?>static/images/ico/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo BASE_URL; ?>static/images/ico/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <script src="<?php echo BASE_URL; ?>static/js/modernizr.custom.js"></script>

        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Tangerine:400,700' rel='stylesheet' type='text/css'>

        <!-- STYLES -->
        <link rel="stylesheet" type="text/css" media="print" href="<?php echo BASE_URL; ?>static/css/print.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/js/nprogress/nprogress.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/css/animate.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/css/fonts/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/css/fonts/fontello/css/fontello.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/css/jquery.fancybox-1.3.4.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/js/google-code-prettify/prettify.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/css/uniform.default.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/css/flexslider.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/js/mediaelement/mediaelementplayer.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/css/tooltipster.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/css/main.css" />

        <!--[if lte IE 9]>
                        <script src="<?php echo BASE_URL; ?>static/js/html5shiv.js"></script>
                        <script src="<?php echo BASE_URL; ?>static/js/respond.min.js"></script>
                        <script src="<?php echo BASE_URL; ?>static/js/selectivizr-min.js"></script>
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>static/css/skins/flat.css" />


        <!-- InstanceBeginEditable name="head" -->
        <!-- InstanceEndEditable -->

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>



    <body>




        <!-- CONTAINER -->
        <div class="container" id="container">



            <!-- HEADER -->
            <?php
            include '_header.php';
            ?>
            <!-- HEADER -->





            <!-- WRAPPER -->
            <div class="wrapper">



                <!-- InstanceBeginEditable name="Page-Content" -->


                <!-- PAGE : ABOUT -->
                <?php
                include '_about.php';
                ?>
                <!-- PAGE : ABOUT -->




                <!-- PAGE : PORTFOLIO -->
                <?php
                #include '_portfolio.php';
                ?>                
                <!-- PAGE : PORTFOLIO -->




                <!-- PAGE : CONTACT -->
                <?php
                include '_contact.php';
                ?>                
                <!-- PAGE : CONTACT -->




                <!-- PAGE : RESUME -->
                <?php
                include '_resume.php';
                ?>                
                <!-- PAGE : RESUME -->




                <!-- InstanceEndEditable -->



            </div>
            <!-- WRAPPER -->


        </div>
        <!-- CONTAINER -->





        <!-- InstanceBeginEditable name="Body-End-Content" -->


        <!-- PORTFOLIO SINGLE AJAX CONTENT CONTAINER -->
        <div class="p-overlay"></div>
        <div class="p-overlay"></div>


        <!-- ALERT : used for contact form mail delivery alert -->
        <div class="site-alert animated"></div>


        <!-- InstanceEndEditable -->




        <!-- SCRIPTS -->
        <script src="<?php echo BASE_URL; ?>static/js/jquery-1.10.2.min.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/jquery.address-1.5.min.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/triple.layout.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/smoothscroll.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/nprogress/nprogress.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/fastclick.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/jquery.imagesloaded.min.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/jquery.isotope.min.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/jquery.flexslider-min.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/jquery.fitvids.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/jquery.validate.min.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/jquery.uniform.min.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/jquery.fancybox-1.3.4.pack.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/jquery.tooltipster.min.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/google-code-prettify/prettify.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/twitterFetcher_v10_min.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/mediaelement/mediaelement-and-player.min.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/send-mail.js"></script>
        <script src="<?php echo BASE_URL; ?>static/js/classie.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
        <script src="<?php echo BASE_URL; ?>static/js/main.js"></script>




        <?php include('_analytics.php'); ?>

    </body>
    <!-- InstanceEnd --></html>
