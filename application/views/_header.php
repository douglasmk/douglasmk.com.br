<header class="header">

    <img src="<?php echo BASE_URL; ?>static/images/site/photo_avatar.png" alt="avatar" />
    <h1>Douglas MK</h1>
    <p>desenvolvedor web</p>

    <!-- NAV MENU -->
    <ul class="vs-nav">

        <!-- InstanceBeginEditable name="Menu-Content" -->

        <li><a href="#/about">sobre</a></li>
        <!--li><a href="#/portfolio">portfolio</a></li-->
        <li><a href="#/contact">contato</a></li>
        <li><a href="#/resume">curriculo</a></li>

        <!-- InstanceEndEditable -->

    </ul>
    <!-- NAV MENU -->

    <!-- SEARCH -->
    <div class="header-search">
        <form role="search" method="get" id="search-form" action="#" />
        <input type="text" value="" name="s" id="search" placeholder="ENTER KEYWORD" />
        <input type="submit" id="search-submit" title="Search" value="→" />
        </form>
    </div>
    <!-- SEARCH -->

</header>